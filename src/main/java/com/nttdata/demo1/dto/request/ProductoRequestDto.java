package com.nttdata.demo1.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductoRequestDto {

    private Integer id;
    private String nombre;
    private Double precio;
    
}
