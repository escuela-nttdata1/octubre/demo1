package com.nttdata.demo1.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductoResponseDto {

    private Integer id;
    private String nombre;
    private Double precio;
}
