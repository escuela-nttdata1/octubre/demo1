package com.nttdata.demo1.respository;

import com.nttdata.demo1.entity.Producto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface IProductoRepository extends JpaRepository<Producto, Integer> {
    
    Optional<Producto> findById(Integer id);
    
    @Query(value = "select p from Producto p where p.nombre = :x")
    List<Producto> listProductoJPQL(@Param("x") String xdasdas);

    @Query(value = "select p from Producto p where p.nombre = ?1")
    List<Producto> listProductoJPQL2(String nombre);

    @Query(value = "select  * from producto where nombre = ?1", nativeQuery = true)
    List<Producto> listProductoNativo(String nombre);


    List<Producto> findByNombreLike(String nombre);
    
}
