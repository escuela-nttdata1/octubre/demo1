package com.nttdata.demo1.respository;

import com.nttdata.demo1.entity.Empresa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;


public interface IEmpresaRepository extends JpaRepository<Empresa, Integer> {
}
