package com.nttdata.demo1.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
@Getter // genera todos los getters
@Setter // genera todos los setters
@AllArgsConstructor// genera constructor con todos sus atributos
@NoArgsConstructor// genera constructor vacio
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"},ignoreUnknown = true)
public class Empresa implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    
    private String razonSocial;
    
    private String ruc;
    
    private String representante;
    
    private Date fechaCreacion;
    
    @OneToMany(mappedBy = "empresa")
    @JsonIgnoreProperties("empresa")
    private Set<Empleado> empleados;
    
    @PrePersist
    public void setDefaultValues(){
        if(Objects.isNull(fechaCreacion)){
            fechaCreacion = new Date();
        }
       
    }
}
