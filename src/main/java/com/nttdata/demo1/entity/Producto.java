package com.nttdata.demo1.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Getter // genera todos los getters
@Setter // genera todos los setters
@AllArgsConstructor// genera constructor con todos sus atributos
@NoArgsConstructor// genera constructor vacio
public class Producto {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String nombre;
    private Double precio;
    private Integer cantidad;
    private LocalDateTime fechaCreacion;
    private Integer estado;
    
    public Producto(Integer id,String nombre){
        this.id = id;
        this.nombre = nombre;
    }

}
