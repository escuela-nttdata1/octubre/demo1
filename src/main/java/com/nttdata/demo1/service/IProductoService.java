package com.nttdata.demo1.service;

import com.nttdata.demo1.dto.request.ProductoRequestDto;
import com.nttdata.demo1.dto.response.ProductoResponseDto;
import com.nttdata.demo1.entity.Producto;

import java.util.List;

public interface IProductoService {
    
    List<ProductoResponseDto> listAll(Integer idProducto);
    
    ProductoResponseDto save(ProductoRequestDto request);

    ProductoResponseDto update(ProductoRequestDto request);
    
    void delete(Integer id);
    
    ProductoResponseDto getProductoById (Integer id);

    List<Producto> listAllProducto(String nombre);
}
