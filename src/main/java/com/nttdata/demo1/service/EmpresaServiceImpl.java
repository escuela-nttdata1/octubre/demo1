package com.nttdata.demo1.service;

import com.nttdata.demo1.entity.Empleado;
import com.nttdata.demo1.entity.Empresa;
import com.nttdata.demo1.respository.IEmpleadoRepository;
import com.nttdata.demo1.respository.IEmpresaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmpresaServiceImpl implements IEmpresaService{

    @Autowired
    private IEmpresaRepository empresaRepository;

    @Override
    public List<Empresa> list() {
        return empresaRepository.findAll();
    }

    @Override
    public void save(Empresa empresa) {
        empresaRepository.save(empresa);
    }
}