package com.nttdata.demo1.service;

import java.util.List;

public interface CrudService<T> {
    
    List<T> list();
    
    void save(T t);
}
