package com.nttdata.demo1.service;

import com.nttdata.demo1.entity.Empresa;
import org.springframework.stereotype.Service;

import java.util.List;


public interface IEmpresaService extends CrudService<Empresa>{
    
  
}
