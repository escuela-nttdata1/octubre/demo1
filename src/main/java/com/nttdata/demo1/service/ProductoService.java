package com.nttdata.demo1.service;

import com.nttdata.demo1.dto.request.ProductoRequestDto;
import com.nttdata.demo1.dto.response.ProductoResponseDto;
import com.nttdata.demo1.entity.Producto;
import com.nttdata.demo1.respository.IProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductoService implements IProductoService {

    @Autowired
    private IProductoRepository productoRepository;

    @Override
    public List<ProductoResponseDto> listAll(Integer idProducto) {
//        return this.productoRepository.listarProductos().stream().filter(p-> Objects.isNull(idProducto) || Objects.equals(p.getId(), idProducto))
//                .map(p->{
//            ProductoResponseDto productoResponseDto = new ProductoResponseDto();
//            productoResponseDto.setId(p.getId());
//            productoResponseDto.setNombre(p.getNombre());
//            productoResponseDto.setPrecio(p.getPrecio());
//            return productoResponseDto;
//        }).collect(Collectors.toList());

        return this.productoRepository.findAll().stream()
                .map(p -> {
                    ProductoResponseDto productoResponseDto = new ProductoResponseDto();
                    productoResponseDto.setId(p.getId());
                    productoResponseDto.setNombre(p.getNombre());
                    productoResponseDto.setPrecio(p.getPrecio());
                    return productoResponseDto;
                }).collect(Collectors.toList());
    }

    @Override
    public ProductoResponseDto save(ProductoRequestDto request) {
        Producto producto = new Producto();

        producto.setNombre(request.getNombre());
        producto.setPrecio(request.getPrecio());
        this.productoRepository.save(producto);

        ProductoResponseDto response = new ProductoResponseDto();
        response.setPrecio(producto.getPrecio());
        response.setNombre(producto.getNombre());
        response.setId(producto.getId());
        return response;
    }

    @Override
    public ProductoResponseDto update(ProductoRequestDto request) {
        Producto producto = new Producto();
        producto.setId(request.getId());
        producto.setNombre(request.getNombre());
        producto.setPrecio(request.getPrecio());
        this.productoRepository.save(producto);

        ProductoResponseDto response = new ProductoResponseDto();
        response.setPrecio(producto.getPrecio());
        response.setNombre(producto.getNombre());
        response.setId(producto.getId());
        return response;
    }

    @Override
    public void delete(Integer id) {
        this.productoRepository.deleteById(id);
    }

    @Override
    public ProductoResponseDto getProductoById(Integer id) {
        // ifPresentOrElse
        ProductoResponseDto productoResponseDto = new ProductoResponseDto();
        Optional<Producto> producto = this.productoRepository.findById(id);
        if(producto.isPresent()){
            productoResponseDto.setId(producto.get().getId());
            productoResponseDto.setNombre(producto.get().getNombre());
            productoResponseDto.setPrecio(producto.get().getPrecio());
        }

        return productoResponseDto;
    }

    @Override
    public List<Producto> listAllProducto(String nombre) {
        return this.productoRepository.listProductoNativo(nombre);
    }
}
