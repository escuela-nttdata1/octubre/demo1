package com.nttdata.demo1.service;

import com.nttdata.demo1.entity.Empleado;
import com.nttdata.demo1.respository.IEmpleadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EmpleadoServiceImpl implements IEmpleadoService{
    
    @Autowired
    private IEmpleadoRepository empleadoRepository;
    
    @Override
    public List<Empleado> list() {
        return empleadoRepository.findAll();
    }

    @Override
    public void save(Empleado empleado) {
        empleadoRepository.save(empleado);
    }
}
