package com.nttdata.demo1.controller;

import com.nttdata.demo1.dto.request.ProductoRequestDto;
import com.nttdata.demo1.dto.response.ProductoResponseDto;
import com.nttdata.demo1.service.IProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("producto")
public class ProductoController {
    
    @Autowired
    private IProductoService productoService;
    
    
    @GetMapping()
    public ResponseEntity<List<ProductoResponseDto>> listAll(@RequestParam(name = "id", required = false) Integer idProducto){
        return new ResponseEntity<>(this.productoService.listAll(idProducto), HttpStatus.OK);
    }

    @GetMapping("/{id}/id")
    public ResponseEntity<ProductoResponseDto> listAll2(@PathVariable("id") Integer idProducto){
        return new ResponseEntity<>(this.productoService.getProductoById(idProducto), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<?> save(@RequestBody ProductoRequestDto request){
        return new ResponseEntity<>(this.productoService.save(request), HttpStatus.CREATED);
    }

    @PutMapping()
    public ResponseEntity<?> update(@RequestBody ProductoRequestDto request){
        return new ResponseEntity<>(this.productoService.update(request), HttpStatus.CREATED);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Integer id){
        this.productoService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


    @GetMapping("query")
    public ResponseEntity<?> listAll2(@RequestParam(name = "nombre", required = false) String nombre){
        return new ResponseEntity<>(this.productoService.listAllProducto(nombre), HttpStatus.OK);
    }
//
//
//    @PutMapping()
//    public ResponseEntity<List<ProductoResponseDto>> update(){
//        return new ResponseEntity<>(this.productoService.listAll(), HttpStatus.OK);
//    }
//
//
//    @DeleteMapping()
//    public ResponseEntity<List<ProductoResponseDto>> delete(){
//        return new ResponseEntity<>(this.productoService.listAll(), HttpStatus.OK);
//    }




}
