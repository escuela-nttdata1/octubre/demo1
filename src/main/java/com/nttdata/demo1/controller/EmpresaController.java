package com.nttdata.demo1.controller;

import com.nttdata.demo1.entity.Empleado;
import com.nttdata.demo1.entity.Empresa;
import com.nttdata.demo1.service.IEmpleadoService;
import com.nttdata.demo1.service.IEmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("empresa")
public class EmpresaController {


    @Autowired
    private IEmpresaService empresaService;


    @GetMapping()
    public ResponseEntity<?> listAll(){
        return new ResponseEntity<>(this.empresaService.list(), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<?> save(@RequestBody Empresa request){
        this.empresaService.save(request);
        return new ResponseEntity<>( HttpStatus.CREATED);
    }
}
