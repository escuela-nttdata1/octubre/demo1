package com.nttdata.demo1.controller;

import com.nttdata.demo1.dto.request.ProductoRequestDto;
import com.nttdata.demo1.dto.response.ProductoResponseDto;
import com.nttdata.demo1.entity.Empleado;
import com.nttdata.demo1.service.IEmpleadoService;
import com.nttdata.demo1.service.IProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("empleado")
public class EmpleadoController {

    @Autowired
    private IEmpleadoService empleadoService;


    @GetMapping()
    public ResponseEntity<?> listAll(){
        return new ResponseEntity<>(this.empleadoService.list(), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<?> save(@RequestBody Empleado request){
        this.empleadoService.save(request);
        return new ResponseEntity<>( HttpStatus.CREATED);
    }
}
